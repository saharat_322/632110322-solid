using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackHitBox : MonoBehaviour
{
    public AttackController attackController;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.gameObject != attackController.gameObject)
        {
            if (collider.gameObject.TryGetComponent<IDamageable>(out var _damageable))
            {
                _damageable.ApplyDamage(gameObject, attackController.currentWeapon.Damage);
            }
        }
    }
   
}
