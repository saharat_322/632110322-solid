using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHealable 
{
    public void Heal(GameObject _source, int _value);
    public void DecreaseHp(int _value);
}
