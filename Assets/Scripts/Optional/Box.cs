using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour,IDamageable
{
    [SerializeField] private int score = 1;
    [SerializeField] protected int currentHp = 1;

    public virtual void ApplyDamage(GameObject _source, int _damage)
    {
        currentHp -= _damage;
        if (currentHp <= 0)
        {
            Death(_source);
            AddScore(score);
        }
    }

  public void AddScore(int _score)
    {
        ScoreManager.Instance.AddScore(_score);
    }

    protected virtual void Death(GameObject _source)
    {
        Destroy(gameObject);
    }

}

