using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolyWater : MonoBehaviour
{
    private Dictionary<GameObject, Coroutine> HealDict = new Dictionary<GameObject, Coroutine>();
   
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.TryGetComponent<IHealable>(out var _healable))
        {
            HealDict[col.gameObject] = StartCoroutine(IEHeal(_healable));
        }
    }
     private void OnTriggerExit2D(Collider2D other)
    {
        if (HealDict.TryGetValue(other.gameObject, out var _coroutine))
        {
            StopCoroutine(_coroutine);
            HealDict.Remove(other.gameObject);
        }
    }
    IEnumerator IEHeal(IHealable _healable)
    {
        while (true)
        {
            _healable.Heal(gameObject, 1);

            yield return new WaitForSeconds(0.5f);
        }
    }
}
